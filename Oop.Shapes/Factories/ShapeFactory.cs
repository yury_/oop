﻿using System;

namespace Oop.Shapes.Factories
{
    public class ShapeFactory
    {
        public Shape CreateCircle(int radius)
        {
            if (radius <= 0)
            {
                throw new ArgumentOutOfRangeException("Radius should be greater than 0");
            }

            var circle = new Circle(radius);        
            
            
            return circle;           
        }

        public Shape CreateTriangle(int a, int b, int c)
        {
            if (a <= 0 || b <= 0 || c <= 0)
			{
                throw new ArgumentOutOfRangeException("Each of sides should be greater than 0");
            }

            if (Triangle.CanNotBeCreated(a, b, c))
            {
                throw new InvalidOperationException("Impossible triangle");
            }

            var triangle = new Triangle(a, b, c);           
            return triangle;
        }

        public Shape CreateSquare(int a)
        {
            if (a <= 0)
            {
                throw new ArgumentOutOfRangeException("Side should be greater than 0");
            }

            var square = new Square(a);
            return square;
        }        

        public Shape CreateRectangle(int a, int b)
        {
            if (a <= 0 || b <= 0)
            {
                throw new ArgumentOutOfRangeException("Each of sides should be greater than 0");
            }

            var rectangle = new Rectangle(a, b);            
            return rectangle;
        }
    }
}