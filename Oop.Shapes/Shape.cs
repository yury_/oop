﻿namespace Oop.Shapes
{
    public abstract class Shape
    {
        /// <summary>
        /// Площадь фигуры
        /// </summary>
        public double Area { get; protected set; }

        /// <summary>
        /// Периметр
        /// </summary>
        public double Perimeter { get; protected set; }

        /// <summary>
        /// Количество вершин
        /// </summary>
        public int VertexCount { get; protected set; }

        public virtual bool IsEqual(Shape shape)
        {
            return false;
        }
    }

    public class Circle : Shape
    {
        public Circle(int radius)
        {
            Radius = radius;
            Area = System.Math.PI * radius * radius;
            Perimeter = System.Math.PI * 2 * Radius;
        }
        public double Radius { get; }

        // Окружность равна другой окружности с таким же радиусом.

        public override bool IsEqual(Shape shape)
        {
            var circle = shape as Circle;
            if (circle == null)
            {
                return false;
            }

            return this.Radius == circle.Radius;
        }
    }

    public class Triangle : Shape
    {
        public Triangle(int sideA, int sideB, int sideC)
        {
            SideA = sideA;
            SideB = sideB;
            SideC = sideC;           
            Perimeter = sideA + sideB + sideC;
            Area = System.Math.Sqrt(Perimeter / 2 * (Perimeter / 2 - sideA) * (Perimeter / 2 - sideB) * (Perimeter / 2 - sideC));
            VertexCount = 3;            
        }
        public static bool CanNotBeCreated(int sideA, int sideB, int sideC) 
        {
            return (sideA + sideB <= sideC || sideA + sideC <= sideB || sideB + sideC <= sideA);
        }
        public double SideA { get; }
        public double SideB { get; }
        public double SideC { get; }

        // Треугольник равен другому треугольнику с такими же сторонами.

        public override bool IsEqual(Shape shape)
        {
            var triangle = shape as Triangle;
            if (triangle == null)
            {
                return false;
            }
            return ((this.SideA == triangle.SideA) && (this.SideB == triangle.SideB) && (this.SideC == triangle.SideC))
                   || ((this.SideA == triangle.SideC) && (this.SideA == triangle.SideB) && (this.SideB == triangle.SideC))
                   || ((this.SideA == triangle.SideB) && (this.SideB == triangle.SideC) && (this.SideC == triangle.SideA))
                   || ((this.SideA == triangle.SideC) && (this.SideB == triangle.SideA) && (this.SideC == triangle.SideB))
                   || ((this.SideB == triangle.SideB) && (this.SideC == triangle.SideA) && (this.SideA == triangle.SideC))
                   || ((this.SideC == triangle.SideC) && (this.SideA == triangle.SideB) && (this.SideB == triangle.SideA))
                   || ((this.SideC == triangle.SideA) && (this.SideB == triangle.SideB) && (this.SideA == triangle.SideC))
                   || ((this.SideB == triangle.SideA) && (this.SideC == triangle.SideC) && (this.SideA == triangle.SideB))
                   || ((this.SideB == triangle.SideC) && (this.SideA == triangle.SideA) && (this.SideC == triangle.SideB));
            
        }
    }

    public class Square : Shape
    {
        public Square(int sideA)
        {
            SideA = sideA;
            SideB = sideA;
            VertexCount = 4;
            Area = sideA * sideA;
            Perimeter = 2 * sideA + 2 * sideA;
        }     

        
        public double SideA { get; }
        public double SideB { get; protected set; }

        // Квадрат равен другому квадрату с такой же стороной.
        public override bool IsEqual(Shape shape)
        {
            var rectangle = shape as Square;
            if (rectangle == null)
            {
                return false;
            }
            return ((this.SideA == rectangle.SideA) && (this.SideB == rectangle.SideB))
                || ((this.SideA == rectangle.SideB) && (this.SideB == rectangle.SideA));
        }
    }

    public class Rectangle : Square
    {
        public Rectangle(int sideA, int sideB) : base(sideA)
        {
            SideB = sideB;
            Area = sideA * sideB;
            Perimeter = 2 * sideA + 2 * sideB;
        }


    }
}
